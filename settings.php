<?php

$output    = '';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

$Viewer = new Viewer_Manager;
$template = file_get_contents(dirname(__FILE__).'/template/settings.html');

$arr_default = array(
    "limit" => 1,
    "modules" => implode('|', $config['action']['allow'][0]),
    "nodes" => ""
);
$config = array_merge($arr_default, $config);

if (isset($_POST['limit'])) {
    foreach (array_keys($arr_default) as $key) {
        if ($key == "modules") {
            $config['action']['allow'][0] = explode('|', $_POST['modules']);
        } else {
            $config[$key] = $_POST[$key];
        }
    }
    file_put_contents($conf_pach, json_encode($config, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
} else {
    $output .= $Viewer->parseTemplate($template, array('config' => $config));
}

/*
?>




<?php

$output    = '';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);
$nodes_file = dirname(__FILE__).'/nodes.txt';

if (isset($_POST['send'])) {
    $config['limit'] = $_POST['limit'];
    $config['action']['allow'][0] = explode('|', $_POST['modules']);

    file_put_contents($conf_pach, json_encode($config));

    $f = fopen($nodes_file, "w");
    fwrite($f,$_POST['template']);

    $output .= '<div class="warning">Сохранено!<br><br></div>';
}

$nodes = file_get_contents($nodes_file);

$output .= '<form action="" method="POST" enctype="multipart/form-data"><div class="list">';

$output .= '<div class="title">' . $config['title'] . ' - Настройки</div>';

$output .= '<div class="level1">
        <div class="items">
            <div class="setting-item"><div class="title">Дефолтные настройки</div></div>
            <div class="setting-item">
                 <div class="left">Максимальное количество ссылок на каждую статью <span class="comment">-1 если без ограничений</span></div>
                 <div class="right"><input type="text" name="limit" value="' . $config['limit'] . '"></div>
                 <div class="clear"></div>
            </div>
            <div class="setting-item">
                 <div class="left">В каких модулях включить: <span class="comment">например, news|stat|loads</span></div>
                 <div class="right"><input type="text" name="modules" value="' . implode('|', $config['action']['allow'][0]) . '"></div>
                 <div class="clear"></div>
            </div>
            <div class="setting-item"><div class="title">http://site.lh/ key1|key2|key3</div></div>
            <div class="setting-item">
                  <textarea name="template" style="width: 99%; height: 350px">' . (isset($nodes) ? h($nodes) : '') . '</textarea>
            </div>
            <div class="setting-item">
                <div class="left"></div>
                <div class="right"><input class="save-button" type="submit" name="send" value="Сохранить"></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>';
    
$output .= '</div></form>';

$output .= '<div class="list"><div class="title">Автор плагина <b>modos189</b></div><div>';
*/
?>