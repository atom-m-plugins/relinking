<?php

class relinking {

    private $config;

    public function __construct($params) {
        $conf_pach = dirname(__FILE__).'/config.json';
        $this->config = json_decode(file_get_contents($conf_pach), true);
    }

    public function common($text, $huk, $type) {
        if ($type == true) return $text; // пропуск комментов

        $nodes = $this->getNodes();
        $limit = $this->config['limit'];

        foreach ($nodes as $node) {
            //$pattern = '/('.implode('|', $node[1]).')/'; // регистрозависимо
            //$pattern = '/('.implode('|', $node[1]).')/iu'; // регистронезависимо
            if (isset($node[0]) and isset($node[1]) and $node[0] != $_SERVER['REQUEST_URI']) {
                $url = $node[0];
                if ($url{0} == '/')
                    $url = (used_https() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . $url;
                $keys = $node[1];

                foreach ($keys as $k => $key) {
                    if (mb_substr($key, 0, 1) != '$' and mb_substr($key, -1) != '$') {
                        $keys[$k] = "((\w(".$key.")(*SKIP)(*FAIL))|((".$key.")\w(*SKIP)(*FAIL))|(".$key."))";
                    } else if (mb_substr($key, 0, 1) == '$') {
                        $keys[$k] = "(((".$key.")\w(*SKIP)(*FAIL))|(".$key."))";
                    } else {
                        $keys[$k] = "((\w(".$key.")(*SKIP)(*FAIL))|(".$key."))";
                    }
                }

                //array_walk($keys, create_function('&$item', '$item = "((\w(".$item.")(*SKIP)(*FAIL))|((".$item.")\w(*SKIP)(*FAIL))|(".$item."))";'));
                $keys = implode('|', $keys);
                $keys = str_replace('$', '([A-ZА-Яа-яa-zёь]{1,})', $keys);
                $keys = str_replace(array("\r","\n"),"",$keys);
                $pattern = '#\[(.*)[^\]]*\][^\[]+\[/\1\]\K(*SKIP)(*FAIL)|('.$keys.')#iu';
                $text = preg_replace_callback($pattern, create_function('$matches', 'return "[url='.$url.'][b]".$matches[0]."[/b][/url]";'), $text, $limit);
            }
        }

        return $text;
    }

    public function getNodes() {
        $lines = $this->config['nodes'];
        $words = [];
        foreach (explode("\r\n", $lines) as $line) {
            $line = trim($line);
            $line = explode(' ', $line, 2);
            if (isset($line[0]) and isset($line[1]))
                $words[] = [$line[0], explode('|', $line[1])];
        }
        return $words;
    }
}